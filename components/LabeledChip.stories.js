import LabeledChip from './LabeledChip'

export default {
  title: 'Chips',
  component: LabeledChip,
  argTypes: {
    text: {
      control: 'text',
      defaultValue: 'Labeled Chip'
    },
    color: {
      control: {
        type: 'select',
        options: [
          'blue-grey lighten-5',
          'teal lighten-4',
          'primary',
          'secondary',
        ],
      },
      defaultValue: 'blue-grey lighten-5'
    },
  }
}

export const CategoryChips = (arg, { argTypes }) => ({
  components: { LabeledChip },
  props: Object.keys(argTypes),
  template: '<LabeledChip v-bind="$props" />'
})
