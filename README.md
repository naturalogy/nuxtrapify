# nuxtrapify

Nuxt.js PWA boilerplate integrated with Strapi and Vuetify.

Strapi API required:
Start running Strapi with the content type of `article` at localhost:1337 before the steps below.

## Build Setup

```bash
# install dependencies
$ yarn

# serve with hot reload at localhost:3000
$ yarn dev

# build for production and launch server
$ yarn build
$ yarn start

# generate static project
$ yarn generate

# Storybook at localhost:3131
$ yarn story
```
