import colors from 'vuetify/es5/util/colors'
import Sass from 'sass'
import Fiber from 'fibers'

export default {
  env: {
    sitename: process.env.npm_package_name || '',
    description: process.env.npm_package_description || '',
  },

  head: {
    htmlAttrs: {
      lang: 'ja',
      prefix: 'og: http://ogp.me/ns#',
    },
    titleTemplate: '%s | ' + process.env.npm_package_name || '',
    meta: [
      { charset: 'utf-8' },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, shrink-to-fit=no',
      },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || '',
      },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com', crossorigin: '' },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css2?family=Noto+Sans+JP:wght@400;700&display=swap',
      },
    ],
  },

  css: [],

  plugins: [],

  components: true,

  buildModules: ['@nuxtjs/eslint-module', '@nuxtjs/vuetify'],

  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/markdownit',
    '@nuxtjs/pwa',
    '@nuxtjs/strapi',
  ],

  axios: {},

  markdownit: {
    breaks: true,
    injected: true,
    linkify: true,
    preset: 'default',
    runtime: true,
  },

  pwa: {
    manifest: {
      lang: 'ja',
      name: process.env.npm_package_name || '',
      short_name: process.env.npm_package_name || '',
      title: process.env.npm_package_name || '',
      description: process.env.npm_package_description || '',
      'og:title': process.env.npm_package_name || '',
      'og:description': process.env.npm_package_description || '',
      theme_color: '#039be5',
      background_color: '#fff',
    },
  },

  storybook: {
    addons: [
      '@storybook/addon-actions',
      '@storybook/addon-controls',
      '@storybook/addon-knobs',
      '@storybook/addon-notes',
      '@storybook/addon-viewport',
    ],
  },

  strapi: {
    entities: ['articles'],
    url: 'http://localhost:1337',
  },

  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      themes: {
        light: {
          primary: colors.lightBlue.darken1,
          secondary: colors.blueGrey.lighten1,
          accent: colors.grey.darken3,
          error: colors.pink.darken1,
          info: colors.cyan.lighten1,
          success: colors.teal.lighten2,
          warning: colors.amber.darken1,
        },
      },
    },
    treeShake: true,
  },

  build: {
    babel: {
      plugins: [['@babel/plugin-proposal-private-methods', { loose: true }]],
    },
    loaders: {
      scss: {
        implementation: Sass,
        sassOptions: {
          fiber: Fiber,
        },
      },
    },
    postcss: {
      plugins: {},
      preset: {
        autoprefixer: {
          grid: true,
        },
      },
    },
  },
}
